
CONTENTS
--
1 - INTRODUCTION
2 - REQUIREMENTS
3 - LIMITATIONS
4 - DISCLAIMER
5 - INSTALLATION
6 - CONFIGURATION
7 - POSSIBLE FUTURE EXTENSIONS
8 - CREDITS


1. INTRODUCTION
--
This module allows you to use the ServiceObjects.com DOTS FastTax web-service.  The web-service uses the customer's city and state/territory to determine the tax that should be applied.  The configuration allows you to decide whether or not to apply a tax per product, per state.  Finally, the module also allows you to configure which states also require a state sales tax on shipping.

This module is invoked by the core uc_taxes module.  That means you can use this module and the tax rule/condition interface provided by the ubercart core uc_taxes module at the same time.  Just be careful not to set up redundant taxes where both modules end up applying the same tax to a product or shipping fee.

2. REQUIREMENTS
--
This module requires PHP 5 and the installation of the php SoapClient extension.  Pleasee see http://us.php.net/manual/en/soap.setup.php for more details.

3. LIMITATIONS
--
Currently, this module only allows you to get tax rates for the United States.  However, ServiceObjects.com offers, in the same service, SOAP functions with slightly different parameters for tax-rates in Canada.  If you are interested in this, please visit ServiceObjects.com for more information.  You may have to create an account to see the documentation and get a trial License Key.

Finally, you should be aware that it is not beyond impossible for tax-rate retrieval and tax amount calculation to fail under certain circumstances.  Although the ServiceObjects.com website advertises 99.995% availability (http://www.serviceobjects.com/products/commerce/fasttax), there may be situations where your server, for whatever reason, fails to access the service. It could be that your license key for the service expires and your store calculates tax amounts using zero tax rates..  In another scenario, a customer may misspell his/her city name or provide an incorrect zipcode.  Although the module is coded to deal with some human error, it can't cover everything since the variety of human error is too wide.  Therefore, you are strongly encouraged to enable the recording of all tax-applications for later auditing.

4. DISCLAIMER
--
The legal responsibility of making sure you correctly assess sales and use taxes for all transactions is yours and you agree to this by using this module.  The maintainers of this module make no guarantees and bear no responsibility for it.

5. INSTALLATION
--
Install this module by enabling it on the modules page (url: admin/build/modules).

6. CONFIGURATION
--
This module allows you to configure sales and use taxes, per product, per state.  To get to the administration section, go to Admin => Store => Settings (url: admin/store/settings).  From there, you can go to the configuration page for the ServiceObjects.com module.

At the very top, you will be asked to enter your License Key from ServiceObjects.com.

Next, there will be an expandable section for shipping taxes that displays a table of checkboxes, with one per U.S. state/territory.  By default, none of the states are configured as states that tax shipping.

There will be a section for 'Sales Tax' where you can mark a checkbox next to each product type to which you want to apply sales tax.  Right below each checkbox will be an expandable table of checkboxes that allow you to decide the states/territories in which the product tax will be applied.  By default, all of the states are selected.

Lastly, there will be a section that allows you to configure a 'Use Tax', per product, per state, in the same way you configure them for the sales tax.

7. POSSIBLE FUTURE EXTENSIONS
--
Currently, this module records to the database the city-/county-/state-level use and sales tax rates that were applied to each product and shipping line item.  However, there is currently no interface for bringing up this data.

8. CREDITS
--
Original implementor: Ankur Rishi (http://drupal.org/user/11703)
Sponsor: ISL Consulting (http://islco.com)