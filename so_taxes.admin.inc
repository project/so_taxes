<?php

/**
 * Builds main admin congiguration form for so_taxes module.
 * 
 * @return
 * A forms API form array.
 */
function so_taxes_admin_settings_form() {
  $form = array();
  
  $form['so_taxes_rate_retrieval']['so_taxes_license_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Your DOTS FastTax license key'),
    '#default_value' => variable_get('so_taxes_license_key', ''),
    '#description' => t('<a href="http://www.serviceobjects.com/products/commerce/fasttax">Click here</a> to obtain a free trial key or purchase a production key.'),
  );
  
  $form['so_taxes_in_testing_mode'] = array(
    '#type' => 'checkbox',
    '#return_value' => 1,
    '#title' => t('Using free trial key'),
    '#description' => t('If you are using a free trial key, check this box. If you\'re using a production key, make sure this box is unchecked.'),
    '#default_value' => variable_get('so_taxes_in_testing_mode', 1)
  );
  
  $form['so_taxes_keep_records'] = array(
    '#type' => 'checkbox',
    '#title' => t('Keep records of taxes that are applied.'),
    '#default_value' => variable_get('so_taxes_keep_records', 1),
    '#return_value' => 1,
    '#description' => t('This is useful if tax rates change or if an order and the tax amount need to be re-constructed later.')
  );
  
  $form['so_taxes_states_without_shipping_tax'] = so_taxes_states_checkboxes(variable_get('so_taxes_states_without_shipping_tax', array()));
  $form['so_taxes_states_without_shipping_tax']['#type'] = 'fieldset';
  $form['so_taxes_states_without_shipping_tax']['#tree'] = TRUE;
  $form['so_taxes_states_without_shipping_tax']['#theme'] = 'so_taxes_states_checkboxes';
  $form['so_taxes_states_without_shipping_tax']['#title'] = t('Do not apply tax on shipping to the following states (even if the DOTS FastTax web-service says one applies)');
  $form['so_taxes_states_without_shipping_tax']['#description'] = t('Check boxes for states and territories for which you do NOT want to tax shipping.  The web-service will determine if a tax applies and apply it.  However, you can override that determination for certain states by checking the appropriate boxes here to disable taxation of shipment to these states.');
  $form['so_taxes_states_without_shipping_tax']['#collapsible'] = TRUE;
  $form['so_taxes_states_without_shipping_tax']['#collapsed'] = TRUE;
  
  $form['sales_tax'] = array(
    '#type' => 'fieldset',
    '#title' => t('"Sales" tax'),
    '#theme' => 'so_taxes_application_config_form'
  );
  
  $form['sales_tax']['so_taxes_sales_tax_product_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Taxed product types'),
    '#multiple' => TRUE,
    '#options' => uc_product_type_names(),
    '#default_value' => variable_get('so_taxes_sales_tax_product_types', array()),
    '#description' => t('Apply taxes to specific product types.'),
  );
  
  $form['use_tax'] = array(
    '#type' => 'fieldset',
    '#title' => t('"Use" tax'),
    '#theme' => 'so_taxes_application_config_form'
  );
  
  $form['use_tax']['so_taxes_use_tax_product_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Taxed product types'),
    '#multiple' => TRUE,
    '#options' => uc_product_type_names(),
    '#default_value' => variable_get('so_taxes_use_tax_product_types', array()),
    '#description' => t('Apply taxes to specific product types.'),
  );
  
  // offer configuration settings to determine which product types get taxed in which states
  foreach (array('sales_tax', 'use_tax') as $tax_type) {
    $product_type_setting = 'so_taxes_'. $tax_type .'_product_types';
    foreach ($form[$tax_type][$product_type_setting]['#options'] as $product_type => $product_type_name) {
      $var_name = 'so_taxes_'. $tax_type .'_included_states4'. $product_type;
      $form[$tax_type][$var_name] = so_taxes_states_checkboxes(variable_get($var_name, so_taxes_states_default_selection()));
      $form[$tax_type][$var_name]['#type'] = 'fieldset';
      $form[$tax_type][$var_name]['#tree'] = TRUE;
      $form[$tax_type][$var_name]['#theme'] = 'so_taxes_states_checkboxes';
      $form[$tax_type][$var_name]['#title'] = t("Choose states for which to charge a sales tax on the '!prod_class' product class", array('!prod_class' => $product_type_name));
      $form[$tax_type][$var_name]['#collapsible'] = TRUE;
      $form[$tax_type][$var_name]['#collapsed'] = TRUE;
      $form[$tax_type][$var_name]['#prefix'] = '<div style="margin-left: 40px;">';
      $form[$tax_type][$var_name]['#suffix'] = '</div>';
    }
  }
  
  return system_settings_form($form);
}

/**
 * Returns a page with links for downloading files.
 * Currently only gives one link for downloading taxes applied to each line_item_id
 * and order_product_id
 */
function so_taxes_data_export_page() {
  $output = '';
  $output .= t('!download a CSV file detailing tax applications.', array('!download' => l(t('Download'), 'admin/store/settings/so_taxes/export/applications_csv')));
  
  return $output;
}

function so_taxes_export_applications_data() {
  
  $result = db_query('SELECT soapp.*, o.order_status FROM {so_tax_applications} soapp LEFT JOIN {uc_orders} o ON soapp.order_id = o.order_id ORDER BY order_id, application_type DESC, application_id');
  
  header('Content-type: application/csv');
  header('Content-Disposition: attachment; filename="tax_application_data.csv"');
  
  // the first line of the CSV; column names
  $first_line = array();
  $first_line[] = 'order_id';
  $first_line[] = 'order_status';
  $first_line[] = 'application_type';
  $first_line[] = 'application_id';
  $first_line[] = 'city_sales_rate';
  $first_line[] = 'county_sales_rate';
  $first_line[] = 'state_sales_rate';
  $first_line[] = 'city_use_rate';
  $first_line[] = 'county_use_rate';
  $first_line[] = 'state_use_rate';
  
  print implode(",", $first_line) ."\n";
  
  while ($row = db_fetch_object($result)) {
    $line = array();
    foreach ($first_line as $field) {
      $line[] = $row->$field;
    }
    print implode(",", $line) ."\n";
  }
  
  exit;
}

function so_taxes_cache_page() {
	return drupal_get_form('cacheform');
}

function cacheform($form_state) { 
	$form['clearcache'] = array('#type' => 'submit', '#value' => t('Clear Rate Cache'));
	return $form;
}

function cacheform_submit($form, &$form_state) {
	cache_clear_all('*', 'cache_so_tax_rates',TRUE);
}

/**
 * Returns an array of checkboxes with a checkbox for each US state/territory.
 * Re-used when adding options to admin config form for deciding state-by-state 
 * configurations for different tax types.
 *
 * @param
 * An array of zone_id values as defined in the {uc_zones} table by the uc_store module to
 * use as default selections for the config setting option for which this fu is being called.
 *
 * @return
 * An array of forms API arrays, each being a checkbox for a US state/territory.
 */
function so_taxes_states_checkboxes($default_values = array()) {
  // get uc_store module's country_id value for United States
  $country_id = db_result(db_query("SELECT country_id FROM {uc_countries} WHERE country_iso_code_2 = 'US'"));
  
  $states_result = db_query('SELECT zone_id, zone_code FROM {uc_zones} WHERE zone_country_id = %d ORDER BY zone_code', $country_id);
  
  $checkboxes = array();
  
  while ($row = db_fetch_object($states_result)) {
    $checkboxes[$row->zone_id] = array(
      '#type' => 'checkbox',
      '#title' => $row->zone_code,
      '#return_value' => $row->zone_id,
      '#default_value' => $default_values[$row->zone_id]
    );
  }
  
  return $checkboxes;
}



